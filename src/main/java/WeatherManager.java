import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class WeatherManager {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите путь файлу");
        String path = scanner.nextLine();

        WeatherManager weatherManager = new WeatherManager();

        File file = new File(path);

        weatherManager.weatherAnalysis(file);
    }

    File weatherAnalysis(File file) throws IOException {

        File weatherInformation = new File(file.getParent() + "\\weather.txt");

        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader((fileReader));

        ArrayList<Weather> weathers = new ArrayList<>();


        for (int i = 0; i < 10; i++) {
            bufferedReader.readLine();
        }
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {

            Integer year = Integer.valueOf(line.substring(0, 4));
            line = line.substring(4);

            Integer month = Integer.valueOf(line.substring(0, 2));
            line = line.substring(2);

            Integer day = Integer.valueOf(line.substring(0, 2));
            line = line.substring(2);

            Integer hour = Integer.valueOf(line.substring(1, 3));
            line = line.substring(6);

            Float temperature = Float.valueOf(line.substring(0, line.indexOf(',')));
            line = line.substring(line.indexOf(',') + 1);

            Float relativeHumidity = Float.valueOf(line.substring(0, line.indexOf(',')));
            line = line.substring(line.indexOf(',') + 1);

            Float windSpeed = Float.valueOf(line.substring(0, line.indexOf(',')));
            line = line.substring(line.indexOf(',') + 1);

            Float windDirection = Float.valueOf(line);

            Weather weather = new Weather(year, month, day, hour, temperature, relativeHumidity, windSpeed, windDirection);
            weathers.add(weather);

        }


        bufferedReader.close();
        fileReader.close();


        weatherInformation.createNewFile();
        FileWriter fileWriter = new FileWriter(weatherInformation);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        bufferedWriter.write("Средняя температура:" + findTheAverageTemperature(weathers) + "\n");
        bufferedWriter.write("Средняя влажность воздуха:" + findTheAverageRelativeHumidity(weathers) + "\n");
        bufferedWriter.write("Средняя скорость ветра:" + findTheAverageWindSpeed(weathers) + "\n");

        bufferedWriter.write(searchForTheHighestTemperature(weathers) + "\n");
        bufferedWriter.write("Самая низкая влажность:" + searchLowestRelativeHumidity(weathers) + "\n");
        bufferedWriter.write("Самая высокая скорость ветра:" + searchForTheHighestWindSpeed(weathers) + "\n");
        bufferedWriter.write(windDirection(weathers));
        bufferedWriter.close();
        fileWriter.close();

        return weatherInformation;
    }

    Float findTheAverageTemperature(ArrayList<Weather> weathers) {
        float result = 0;

        for (Weather weather : weathers) {
            result += weather.getTemperature();
        }
        return result / weathers.size();
    }

    Float findTheAverageRelativeHumidity(ArrayList<Weather> weathers) {
        float result = 0;
        for (Weather weather : weathers) {
            result += weather.getRelativeHumidity();
        }
        return result / weathers.size();
    }

    Float findTheAverageWindSpeed(ArrayList<Weather> weathers) {
        float result = 0;
        for (Weather weather : weathers) {
            result += weather.getWindSpeed();
        }
        return result / weathers.size();
    }

    String searchForTheHighestTemperature(ArrayList<Weather> weathers) {
        float result = 0;
        int day = 0;
        int hour = 0;
        for (Weather weather : weathers) {
            if (weather.getTemperature() > result) {
                result = weather.getTemperature();
                day = weather.getDay();
                hour = weather.getHour();
            }
        }
        return "Наибольшая температура:" + result + " 2021.03." + day + " час:" + hour + ".00";
    }

    Float searchLowestRelativeHumidity(ArrayList<Weather> weathers) {
        float max = Float.MAX_VALUE;

        for (Weather weather : weathers) {
            if (weather.getRelativeHumidity() < max) {
                max = weather.getRelativeHumidity();
            }
        }
        return max;
    }

    Float searchForTheHighestWindSpeed(ArrayList<Weather> weathers) {
        float result = 0;
        for (Weather weather : weathers) {
            if (result < weather.getWindSpeed()) {
                result = weather.getWindSpeed();
            }
        }
        return result;
    }

    String windDirection(ArrayList<Weather> weathers) {

        int[] side = new int[4];
        for (Weather weather : weathers) {
            if (weather.getWindDirection() >= 45 && weather.getWindDirection() < 135) {
                side[0]++;
            } else if (weather.getWindDirection() >= 135 && weather.getWindDirection() < 225) {
                side[1]++;
            } else if (weather.getWindDirection() >= 225 && weather.getWindDirection() < 315) {
                side[2]++;
            } else {
                side[3]++;
            }
        }
        int max = 0;

        for (int i = 1; i < 3; i++) {
            if (side[i] > max) {
                max = side[i];
            }
        }
        if (side[0] == max) {
            return "Самая частая сторона ветра: Восток";
        }
        if (side[1] == max) {
            return "Самая частая сторона ветра: Юг";
        }

        if (side[2] == max) {
            return "Самая частая сторона ветра: Запад";
        }

        if (side[3] == max) {
            return "Самая частая сторона ветра: Север";
        }

        return "нет";
    }
}






